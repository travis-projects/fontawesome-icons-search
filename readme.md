# FontAwesome Icons Search

A simple package to search through the current FA Icons list, and output an icon.
For example, you could search for 'Facebook', and it would out put the 'facebook'.

A usecase for this: I use it in a Gatsby site, where the CMS has options to add social links, with a name. I get the array of links + names, then search for the correct icon to use for FontAwesome React - rather than hard coding 'Facebook' to match 'facebook', 'Twitter T' to match 'twitter-t' etc. You can simply create a loop, like so:

```javascript
const { searchBrand } = require('fa-icon-search')

// Component
{
  links.map(link => {
    const icon = searchBrand(link.name)
    return (
      <FontAwesomeIcon icon={['fab', icon.name ]} />
    )
  })
}
```

And you can have any number of links + icons, as opposed to:

```javascript
// Component
{
  links.map(link => {
    let icon
    if (link.name === 'Facebook') icon = 'facebook'
    if (link.name === 'twitter') icon = 'twitter'
    return (
      <FontAwesomeIcon icon={['fab', icon]} />
    )
  })
}
```

### Contributing

Any updates are welcome - create a MR in the repository.
