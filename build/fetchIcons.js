const fetch = require('node-fetch')

const createIconFiles = require('./sortIcons')

fetch('https://raw.githubusercontent.com/FortAwesome/Font-Awesome/master/advanced-options/metadata/icons.json')
  .then(res => res.json())
  .then(fileContents => createIconFiles(fileContents))
