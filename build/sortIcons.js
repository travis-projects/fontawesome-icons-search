const fs = require('fs')
const appRoot = require('app-root-path')

const createIconFiles = (icons) => {
  const createFile = (name, content) => {
    const fileContents = JSON.stringify(content)
    fs.writeFile(`${appRoot}/icons/${name}.json`, fileContents, 'utf8', (err) => {
      if (err) {
        return console.log(err);
      }
      console.log(`${name}.json is saved`);
    })
  }

  const brands = Object.entries(icons).filter(icon => icon[1].styles.includes('brands')).map((icon) => ({
    name: icon[0],
    label: icon[1].label
  }))


  const solids = Object.entries(icons).filter(icon => icon[1].styles.includes('solid')).map((icon) => ({
    name: icon[0],
    label: icon[1].label
  }))

  createFile('brands', brands)
  createFile('solids', solids)
}

module.exports = createIconFiles;
