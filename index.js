'use strict'

// Get Icons
const brands = require('./icons/brands.json')
const solids = require('./icons/solids.json')

// Functions
const searchBrand = (label) => brands.find(icon => icon.label.toLowerCase() === label.toLowerCase())
const searchSolid = (label) => solids.find(icon => icon.label.toLowerCase() === label.toLowerCase())

// Export
module.exports = {
  searchBrand,
  searchSolid
}
